package main

import (
	"fmt"
	"net/http"
	"os"
	"strings"
)

func main() {
	port := "80"
	path := "./"

	changePort := false
	isSSL := false
	crtFile := "server.crt"
	keyFile := "server.key"

	if len(os.Args) > 1 {
		for _, v := range os.Args {
			if strings.Contains(strings.ToLower(v), "help") {
				fmt.Println("http or https server by mirwing")
				fmt.Println("useage : ./web [--port:8080] [--path:./]")
				fmt.Println("useage : ./web [--ssl] [--port:8080] [--crt:./server.crt] [--key:./server.key] [--path:./]")
				os.Exit(1)
			}
			if strings.Contains(strings.ToLower(v), "ssl") {
				isSSL = true
			}
			if strings.Contains(strings.ToLower(v), "port") {
				ports := strings.Split(v, ":")
				if len(ports) > 1 {
					changePort = true
					port = ports[1]
				}
			}
			if strings.Contains(strings.ToLower(v), "crt") {
				crts := strings.Split(v, ":")
				if len(crts) > 1 {
					crtFile = crts[1]
				}
			}
			if strings.Contains(strings.ToLower(v), "key") {
				keys := strings.Split(v, ":")
				if len(keys) > 1 {
					keyFile = keys[1]
				}
			}
		}
	}
	if changePort {
		port = fmt.Sprintf(":%s", port)
	} else if isSSL {
		port = fmt.Sprintf(":%d", 443)
	} else {
		port = fmt.Sprintf(":%d", 80)
	}
	if isSSL {
		fmt.Println("Running Https Server Port(", port, ") : ", path)
		panic(http.ListenAndServeTLS(port, crtFile, keyFile, http.FileServer(http.Dir(path))))
	} else {
		fmt.Println("Running Http Server Port(", port, ") : ", path)
		panic(http.ListenAndServe(port, http.FileServer(http.Dir(path))))
	}
}
