# Simple Web Server
useage: ./server [--port:8080] [--path:./]  
useage: ./server [--ssl] [--port:8080] [--crt:./server.crt] [--key:./server.key] [--path:./]  

default port : 80  
default path : ./  

default ssl port : 443  
default crt file : server.crt  
default key file : server.key  
